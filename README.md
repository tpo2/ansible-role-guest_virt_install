# Ansible role for creating libvirt VMs

This role does the following:
- create storage for the VM using LVM
- generate an unattended installation configuration for the target distribution
- if the VM is missing:
  - run the VM with the CPU/RAM/network parameters and start the unattended installation
  - wait for the installation to reboot
  - add the VM SSH key to your local known_hosts file

One example of a playbook to do that:

```
- hosts: virt01.example.org
  roles:
  - role: guest_virt_install
    name: guest.example.org
    system_disk_size: 8
    volgroup: volume01
    mem_size: 2048
    num_cpus: 2
    distribution: Fedora
    version: 22
    bridge: virbr0
    network:
      proto: 'dhcp'
```

# Example 2

Another example using an image in the file system for the system disk:

```
- hosts: virt01.example.org
  roles:
  - role: guest_virt_install
    name: guest.example.org
    system_disk_size: 8
    storage_type: image
    mem_size: 2048
    num_cpus: 2
    distribution: Fedora
    version: 22
    bridge: virbr0
    network:
      proto: 'dhcp'
```

For now, this is a very basic wrapper around virt-install, but more options
are gonna be added.

## Disk layout

By default, the VM will be installed with a single disk used for system.

How the disk are created can be controlled by setting `storage_type`. By default
`storage_type` is `lv` as in "logical volume". If `storage_type` is set to `image`
then an disk image will be created in the file system.

You can decide to add a 2nd disk for data, that will not be erased as part of the
installation, thus permitting to easily reinstall the whole system if needed.

If `storage_type` is `lv` then the disks are created as a logic volume on the
 volume group `volgroup` on the server.

You can set the disk sizes with `system_disk_size` and `data_disk_size`, expressed in Gb.

## CPU and Memory

Unless specified otherwise with `num_cpus`, the VM will have 1 CPU. It can be hotplugged to
2 * num_cpus, see libvirt and virsh documentation for how to do it.

The memory must be expressed in Mb, so 1Gb would be 1024. There is no default value as of now. The
role do verify that we do allocate enough memory for installing distributions. Like the cpus, memory
can extended without reboot up to twice the amount of normal memory.

## Limitation

Beware, the system only make sure that the VM is installed and by default start it, but will
not make sure the VM is started if the admin decide to stop it manually or manage it using another
role/playbook. This is a difference with the service module.

## Supported distributions

Anaconda based distributions (Fedora and Centos) are supported and stable.
Debian and Ubuntu support is quite recent (october 2017) and while it was tested, it is much more
recent than anaconda-based ones, so bugs can happen.

Others OS (ie, various BSDs) would requires support in virt-install and a way to inject automated
answers to the installer. There is no straight forward way for most of them.

## Network configuration

In order to configure the network, you need to fill a structure for the `network` argument. There
is 2 types of setup, dhcp and static

### DHCP Network configuration

By default, the role will default to DHCP if no configuration is given.

Previous version of the role would take a `bootproto` entry in the `network` dictionary
but this is now automatically detected and not needed anymore.

### Static Network configuration

For static setup, a `network` structure must be given, with the
`ip` (ip and subnet information) and `gateway` being mandatory keys.

`ip` being the ipv4 address, with the netmask being required.
`gateway` would be the default route, again using ipv4.

Here is a example of the setup:
```
- hosts: virt01.example.org
  roles:
  - role: guest_virt_install
    name: guest.example.org
    ...
    network:
      ip: 10.0.0.15/24
      gateway: 10.0.0.1
```

### DNS settings

In order to simplify parameters sharing, DNS can also be specified as a first level
variable instead of being part of the `network` dictionary. If left unspecified, default
from the role (currently, the Google DNS) would apply. It can be overriden with group_vars
or host_vars, or even on a per host basis.

## Retrieving VM distribution configuration and root password

The VM settings generated by this role can be found under `/etc/libvirt/kickstarts/<vm-name>`
or `/etc/libvirt/preseeds/<vm-name>`, respectively for Red Hat- or Debian-style systems.

This directory contains the following files:
- `ks.cfg`: the kickstart file for Anaconda on Red Hat systems
- `preseed.cfg`: the Debian installer configuration on Debian systems
- `password`: the root password

